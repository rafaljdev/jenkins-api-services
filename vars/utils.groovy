def getEcosystem(fileId) {
    def ecosystem
    configFileProvider([configFile(fileId: "${fileId}", variable: 'ecosystem_json_file')]) {
        ecosystem = readJSON file: "${ecosystem_json_file}"

        if (!ecosystem.containsKey("envs")) {
            error "Invalid ecosystem JSON file"
        }

        if (ecosystem.envs.keySet()?.empty) {
            error "No environments set in ecosystem JSON"
        }

    }
    return ecosystem
}


def generateEnvChoicesString(envs_list) {
    return envs_list.collect { it }.join('\n');
}

def getSharedInputParametersList(ecosystem) {
    def envs_list = ["none"]
    envs_list.addAll(ecosystem.envs.keySet())
    def choices_string = generateEnvChoicesString(envs_list)
    return [
            string(name: 'global_build_number', defaultValue: '0.0.1-SNAPSHOT', description: 'USTA Global Build number'),
            choice(name: 'target_env', choices: "${choices_string}", defaultValue: "none", description: 'Specify environment to deploy to. If none is selected, deployment to AEM will be skipped as long as this branch is not subject for auto-deployment'),
            booleanParam(name: 'skip_tests', defaultValue: false, description: 'Tick to skip all tests')
    ]
}

def getMuleIntegrationTestsInputParametersList(ecosystem) {
    def envs_list = []
    envs_list.addAll(ecosystem.envs.keySet())
    def choices_string = generateEnvChoicesString(envs_list)
    return [
            choice(name: 'target_env', choices: "${choices_string}", defaultValue: "none", description: 'Specify environment where tests should be performed'),
    ]
}

def getMuleInputParametersList(ecosystem, isProdStage) {
    def envs_list = ["none"]
    envs_list.addAll(ecosystem.envs.keySet())
    def choices_string = generateEnvChoicesString(envs_list)
    return [
            choice(name: 'target_env', choices: "${choices_string}", defaultValue: "none", description: 'Specify environment to deploy to. If none is selected, deployment to Mule will be skipped as long as this branch is not subject for auto-deployment'),
            booleanParam(name: 'skip_tests', defaultValue: isProdStage, description: 'Tick to skip all tests')
    ]
}

def getDockerInputParametersList(ecosystem, isProdStage) {
    def envs_list = ["none"]
    envs_list.addAll(ecosystem.envs.keySet())
    def choices_string = generateEnvChoicesString(envs_list)
    return [
            choice(name: 'target_env', choices: "${choices_string}", defaultValue: "none", description: 'Specify environment to deploy to. If none is selected, deployment to Docker will be skipped as long as this branch is not subject for auto-deployment'),
            booleanParam(name: 'skip_tests', defaultValue: isProdStage, description: 'Tick to skip all tests'),
            booleanParam(name: 'skip_integration_tests', defaultValue: isProdStage, description: 'Tick to skip all integration tests')
    ]
}

def getDockerMaintenanceInputParametersList(dockerEcosystem) {
    def envs_list = []
    envs_list.addAll(dockerEcosystem.keySet())
    def choices_string = generateEnvChoicesString(envs_list)
    return [
            choice(name: 'target_env', choices: "${choices_string}", defaultValue: "none", description: 'Specify environment to deploy to. If none is selected, deployment to Docker will be skipped as long as this branch is not subject for auto-deployment'),
            choice(name: 'command', choices: "restart\nstop\nstart", defaultValue: "restart", description: 'Specify environment to deploy to. If none is selected, deployment to Docker will be skipped as long as this branch is not subject for auto-deployment'),
    ]
}

def getDockerEnvironmentsMap(ecosystem) {
    def dockerEcosystem = [none: null]

    for (def system : ecosystem.envs.keySet()) {
        for (def serviceEntry : ecosystem.envs["${system}"]) {
            def service = serviceEntry.getValue()
            if (service.docker != null) {
                def dockers = service.docker.split(',')
                def serviceName = service.serviceName
                for (def docker : dockers) {
                    def key = system + ' - ' + docker + ' - ' + serviceName;
                    def value = [
                            "system" : system,
                            "docker": docker,
                            "serviceName" : serviceName
                    ]
                    dockerEcosystem.put(key,value)
                }
            }
        }
    }

    return dockerEcosystem
}


def generateInputFields(inputProps) {
    properties([
            parameters(inputProps)
    ])
}
