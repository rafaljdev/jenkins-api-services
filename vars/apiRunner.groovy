import api.ApiDockerRunner

def runApiPipeline(params, serviceName, mavenModuleName = serviceName) {
    stage('Checkout sources') {
        checkout scm
    }

    echo "Loading ecosystem..."
    def ecosystem = utils.getEcosystem('usta-mule-ecosystem')


    echo "Creating parameters..."
    def inputProps = []
    inputProps.addAll(utils.getDockerInputParametersList(ecosystem, false))
    utils.generateInputFields(inputProps)

    echo "Running pipeline..."

    def apiDockerRunner = new ApiDockerRunner(this)
    apiDockerRunner.startDockerPipeline(params, ecosystem, serviceName, mavenModuleName)
}