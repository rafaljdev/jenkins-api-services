package api.script

abstract class AbstractScripter {

    def script

    AbstractScripter(def script) {
        this.script = script
    }

    def notifier = script.notifier

    def deleteDir() {
        return script.deleteDir()
    }

    def withMaven(param1, param2) {
        return script.withMaven(param1, param2)
    }

    def stage(param1, param2) {
        return script.stage(param1, param2)
    }

    def dir(param1, param2) {
        return script.dir(param1, param2)
    }

    def sh(param1) {
        return script.sh(param1)
    }

    def echo(param1) {
        return script.echo(param1)
    }

}