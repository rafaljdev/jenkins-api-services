package api

import api.script.AbstractScripter

class ApiDockerRunner extends AbstractScripter {

    ApiDockerRunner(def script) {
        super(script)
    }

    def startDockerPipeline(params, ecosystem, service, mavenModuleName) {
        def apiModule = "api-${mavenModuleName}"

        try {
            runMavenStages(params, apiModule)

            deployDocker(params, ecosystem, service, apiModule)

            notifier.notifySuccess(ecosystem)

        } catch (e) {
            notifier.notifyFailure(ecosystem)
            throw e
        }
        finally {
            deleteDir() /* clean up our workspace */
        }
    }

    def runMavenStages(params, apiModule) {
        def mvnModuleWithDepends = "mvn -am -pl :${apiModule}"

        def skipTests = params.skip_tests
        def skipIntegrationTests = params.skip_integration_tests

        withMaven(maven: 'USTA_Maven') {

            stage('Install for tests') {
                if (!(skipTests)) {
                    sh "${mvnModuleWithDepends} clean install -DskipTests"
                }
            }

            stage('Unit tests') {
                if (!(skipTests)) {
                    sh "${mvnModuleWithDepends} test"
                }
            }

            //todo run context test for actually deployed profile ? or even all profiles ? there is case, where
            //todo profile is not provided
            stage('Integartion tests') {
                if (!(skipTests || skipIntegrationTests)) {
                    sh "${mvnModuleWithDepends} failsafe:integration-test"
                }
            }

            stage('Build package') {
                sh "${mvnModuleWithDepends} package -DskipTests"
            }


            stage('SonarQube analysis') {
                script.withSonarQubeEnv('sonarqube') {
                    sh "${mvnModuleWithDepends} org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar"
                    //sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar'
                }
            }
        }
    }

    def deployDocker(params, ecosystem, service, apiModule) {
        stage('Run Docker Container') {
            dir("${apiModule}") {

                echo "params.target_env: ${params.target_env}"

                if (params.target_env != null && params.target_env != 'none') {

                    def environment = ecosystem.envs["${params.target_env}"]
                    def serviceEnvironments = environment["${service}"]
                    def javaWorkspace = serviceEnvironments.javaWorkspace
                    def dockerWorkspace = serviceEnvironments.dockerWorkspace
                    def port = serviceEnvironments.port
                    def debugPort = serviceEnvironments.debugPort
                    def serviceName = serviceEnvironments.serviceName
                    def internalPort = serviceEnvironments.internalPort
                    def dockers = serviceEnvironments.docker.split(',')

                    sh 'docker build --tag=' + serviceName + ':latest --rm=true .'

                    def serviceTarFile = serviceName + '.tar'
                    sh 'docker save -o ' + serviceTarFile + ' ' + serviceName

                    //todo refactor: dockers is probably IP address ?
                    for (def docker : dockers) {

                        sh 'scp ' + serviceTarFile + ' ubuntu@' + docker + ':/home/ubuntu'

                        sh 'ssh ubuntu@' + docker + ' docker load -i ' + serviceTarFile

                        sh 'ssh ubuntu@' + docker + ' rm /home/ubuntu/' + serviceTarFile
                        try {
                            sh 'ssh ubuntu@' + docker + ' docker stop ' + serviceName

                            sh 'ssh ubuntu@' + docker + ' docker rm ' + serviceName
                        } catch (e) {
                            e.printStackTrace()
                        }

                        //todo --memory value from env ?

                        sh 'ssh ubuntu@' + docker +
                                ' docker run -d' +
                                ' --name=' + serviceName +
                                ' --restart always ' +
                                ' --publish=' + port + ':' + internalPort +
                                ' --publish=' + debugPort + ':8000' +
                                ' --memory="340M"' +
                                ' -v ' + dockerWorkspace + ':' + javaWorkspace +
                                ' ' + serviceName + ':latest'
                    }
                }
            }
        }
    }

}